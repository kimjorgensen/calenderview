const table = document.querySelector('.calendar-table');

var tableDatas = [];

let dragStart = 0;
let dragEnd = 0;
let isDragging = false;

const HOURS = 24;
const COLORS = {
    1: '#f2d7ab',
    2: '#d3f1d7',
    3: '#f1e1d3',
    4: '#f1d3d3',
    5: '#dcd8f2'
}

function displayQTip(td, showQTip)
{			
    var html = $('#pupup_schedule').html();
	
    if(!$(td).hasClass('qtip')) {
		$(td).qtip({
			content: {
				prerender: true, // important
				text: html
			},
			position: {
				at: 'bottom center',
				my: 'top center',
				viewport: $(window) // Keep it on-screen at all times if possible
			},
			show: {
				event: 'click', 
				solo: true,  
			},
			hide: 'unfocus', // Hide when the tooltip loses focus
			style: {
				classes: 'ui-tooltip-rounded ui-tooltip-shadow ui-tooltip-jtools',
				width: 'auto', height: 'auto'
			},
			events: {
				render: function(event, api) { },
				hide: function(event, api){ }
			},
		});
	}
	
	if (showQTip)
		$(td).qtip('show');
		
}

function hideQTip() {
	$('.qtip').each(function(){
	   $(this).qtip('hide');
	});
}

/**
 * Return htmlCollection as an array.
 * @param {htmlCollection} htmlCollection 
 */
function toArray(htmlCollection) {
    return [].slice.call(htmlCollection)
}

/**
 * Set value of all selected table data.
 * @param {number} value 
 */
function setValues(value) {
	tableDatas.forEach(function(element) {
		if (isSelected(element)) {
			element.innerText = value
			element.style.backgroundColor = COLORS[value]
		}
		let setupNumber = element.innerText
		let dayValue = Math.floor(tableDatas.indexOf(element) / HOURS)
		let hourValue = tableDatas.indexOf(element) % HOURS
		
		//console.log(setupNumber + " - " + dayValue + " - " + hourValue)
		
		$.post('profile.php', { action:'cds', sn:setupNumber, d: dayValue, h:hourValue }, function(result) {  });
	})
	
	hideQTip()
	
	clearSelection()
}

/**
 * Check if mouse key pressed is right-click.
 * @param {event} event 
 */
function isRightClick(event) {
    // Return true if mouse key pressed is right-click.
    if (event.which) {
        return (event.which === 3)
    } else if (event.button) {
        return (event.button === 2)
    }
    // Return false if mouse key pressed is not right-click.
    return false;
}

/**
 * Check if current state of table data is selected
 * (Return if class name is 'selected').
 * @param {object} tableData 
 */
function isSelected(tableData) {
    return tableData.className === 'selected'
}

/**
 * Start multi-selection of table datas.
 * @param {event} event 
 */
function toggleSelectedMouseDown(event) {
    // !Escape if right click is pressed.
    if (isRightClick(event)) return;
    // Start new selection.
    clearSelection()
    // Set start position of drag.
    dragStart = tableDatas.indexOf(event.target)
	// Set starting point
	tableDatas[dragStart].className = 'selected'
    // Start dragging.
    isDragging = true
    // Prevent undefined.
    if (typeof event.preventDefault != 'undefined') {
        event.preventDefault()
    }
}

/**
 * Stop multi-selection of table datas.
 * @param {event} event 
 */
function toggleSelectedMouseUp(event) {
    // !Escape if right click is pressed.
    if (isRightClick(event)) return;
    // End dragging.
	isDragging = false
}

/**
 * Change selected table datas when moving the mouse.
 * @param {event} event 
 */
function setSelectedRange(event) {
    // Check if user is still dragging the mouse.
    if (isDragging) {
        // Get the last index of selected table datas.
        dragEnd = tableDatas.indexOf(event.target)
        // Set selected state on table datas.
        selectRange(tableDatas)
    }
}

/**
 * Set selected state on table datas
 */
function selectRange(tableDatas) {
    const startCol = dragStart % HOURS
    const endCol = dragEnd % HOURS;
    const startRow = Math.floor(dragStart / HOURS)
    const endRow = Math.floor(dragEnd / HOURS)

    clearSelection()

    for (let row = Math.min(startRow, endRow); row <= Math.max(startRow, endRow); row++) {
        for (let col = Math.min(startCol, endCol); col <= Math.max(startCol, endCol); col++) {
            tableDatas[row * HOURS + col].className = 'selected'
        }
	}
}

/**
 * Clear selected state of all table data
 * (Set class name of all table data to empty).
 */
function clearSelection() {
	tableDatas.forEach(function(element) {
		element.className = ''
	})
}

/**
 * Return the number of properties in object.
 * @param {object} object 
 */
function countProperties(object) {
    return Object.keys(object).length
}

/**
 * Check if current data field is considered a table header
 * (Applies to data fields containing hours and days).
 * @param {number} row 
 * @param {number} col 
 */
function isTableHeader(row, col) {
    return row === 0 || col === 0
}

/**
 * Set hour or day value in current data field.
 * @param {number} row 
 * @param {number} col 
 */
function renderTableHeader(row, col) {
    // Return empty if it's the first data field.
    if (row === 0 && col === 0) return ''
    // Return hour if it's the first row.
    if (row === 0) return col <= 10 ? '0' + (col - 1) : col - 1
    // Return day if it's the first column.
    if (col === 0) return DAYS[row]
}

/**
 * Draw the table
 * Add mouse listeners to each table data.
 * @param {number[]} values 
 */
function render(values) {
	console.log(JSON.stringify(mySchedulers));
	
	$('.calendar-table td').each(function() {
		tableDatas.push(this);
		
		$(this).mousedown(function(event) {
			toggleSelectedMouseDown(event)
		})
		$(this).mousemove(function(event) {
			// Check if mouse is still pressed.
            if (isDragging) {
                // Change selected table datas.
                setSelectedRange(event)
            }
		})
		$(this).mouseup(function(event) {
			// Stop multi-selection at current table data.
            toggleSelectedMouseUp(event)
			displayQTip(this, true);
		})
	})
}

$(function() {
	render([]);
});